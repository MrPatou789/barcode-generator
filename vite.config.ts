import { resolve } from 'path'
import { rmSync } from 'fs'
import { defineConfig } from 'vite'

import vue from '@vitejs/plugin-vue'
import electron from 'vite-plugin-electron'
import renderer from 'vite-plugin-electron-renderer'
import pkg from './package.json'

import autoImport from 'unplugin-auto-import/vite'
import vueI18n from '@intlify/unplugin-vue-i18n/vite'
import vuetify from 'vite-plugin-vuetify'

rmSync('dist-electron', { recursive: true, force: true })
const isBuild = process.argv.slice(2).includes('build')

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vuetify({ autoImport: true }),
    vueI18n({
      include: resolve(__dirname, 'locales/**'),
    }),
    autoImport({
      dts: 'src/auto-imports.d.ts',
      imports: ['vue', 'vue-i18n'],
      dirs: ['src/composables'],
    }),
    electron([
      {
        // Main-Process entry file of the Electron App.
        entry: 'electron/main/index.ts',
        onstart(options) {
          options.startup()
        },
        vite: {
          build: {
            minify: isBuild,
            outDir: 'dist-electron/main',
            rollupOptions: {
              external: Object.keys(pkg.dependencies),
            },
          },
        },
      },
      {
        entry: 'electron/preload/index.ts',
        onstart(options) {
          // Notify the Renderer-Process to reload the page when the Preload-Scripts build is complete,
          // instead of restarting the entire Electron App.
          options.reload()
        },
        vite: {
          build: {
            minify: isBuild,
            outDir: 'dist-electron/preload',
            rollupOptions: {
              external: Object.keys(pkg.dependencies),
            },
          },
        },
      },
    ]),
    // Use Node.js API in the Renderer-process
    renderer({
      nodeIntegration: true,
    }),
  ],
  resolve: {
    alias: {
      '@Components': resolve(__dirname, 'src/components'),
    },
  },
  clearScreen: false,
})

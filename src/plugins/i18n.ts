import { createI18n } from 'vue-i18n'
import messages from '@intlify/unplugin-vue-i18n/messages'

export default createI18n({
  legacy: false,
  locale: import.meta.env.VITE_APP_I18N_LOCALE || 'fr',
  fallbackLocale: import.meta.env.VITE_APP_I18N_FALLBACK_LOCALE || 'fr',
  messages,
})

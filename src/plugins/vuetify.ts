// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'
import type { ThemeDefinition } from 'vuetify'

import { fr } from 'vuetify/locale'

const customLightTheme: ThemeDefinition = {
  dark: false,
  colors: {
    'primary': '#CDDC39',
    'primary-lighten-4': '#f5f5f5',
    'secondary': '#bdbdbd', // Vert lime
  },
}

export default createVuetify({
  locale: {
    locale: import.meta.env.VITE_APP_I18N_LOCALE || 'fr',
    fallback: import.meta.env.VITE_APP_I18N_FALLBACK_LOCALE || 'fr',
    messages: { fr },
  },
  theme: {
    defaultTheme: 'customLightTheme',
    themes: {
      customLightTheme,
    },
  },
  defaults: {
    VBtn: { rounded: 'pill' },
    VTextField: { variant: 'solo' },
    VAutocomplete: { variant: 'solo' },
  },
})
// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides

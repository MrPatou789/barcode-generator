import { customAlphabet, nanoid } from 'nanoid'

const useId = {
  generate: () => nanoid(),
  generateFromAlphabet: () => {
    const nanoid = customAlphabet('azertyuiopqsdfghjklmwxcvbn', 10)
    return nanoid()
  },
  generateFromPattern: (pattern: string, index: number) => {
    if (!pattern) return useId.generate()

    pattern = pattern.replace(/{{nanoid}}/, useId.generate())
    pattern = pattern.replace(/{{index}}/, index.toString())

    return pattern
  },
}

export { useId }
